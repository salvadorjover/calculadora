# README #

El Blog de Delhi Básico: Salvador Jover

El código forma parte de una serie de entradas del blog de Delphi Básico, con fines exclusivamente didácticos 
y se entrega tal cual; no tiene carácter comercial ni ofrece garantías de que funcione fuera del uso meramente educativo.

El repositorio contiene el código vinculado a la ultima de las entradas de la serie sobre la creación de una 
calculadora con Delphi.

CALCULADORA (I)
https://delphibasico.wordpress.com/2015/02/09/crea-tu-propia-calculadora-con-delphi-parte-i/

CALCULADORA (II)
https://delphibasico.wordpress.com/2015/02/10/crea-tu-propia-calculadora-con-delphi-parte-ii/

CALCULADORA (III)
https://delphibasico.wordpress.com/2015/02/11/crea-tu-propia-calculadora-con-delphi-parte-iii/

CALCULADORA (IV)
https://delphibasico.wordpress.com/2015/02/15/crea-tu-propia-calculadora-con-delphi-parte-iv/

CALCULADORA (Y V) APÉNDICE
https://delphibasico.wordpress.com/2015/02/17/crea-tu-propia-calculadora-con-delphi-apendice-parte-iv/

Objetivo:
Serie que aborda la tarea de construir una calculadora con XE7, como componente. Estamos celebrando el 20 Aniversario de 
Delphi durante la semana 9 al 13 de Febrero. #DelphiWeek. (2015)

Video que acompaña la entrada:
https://videopress.com/v/nHQcXk2o


