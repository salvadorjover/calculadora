unit UDBCalculadora;

interface

uses System.SysUtils, System.Variants, System.Classes, Generics.Collections,
  UDBClasses;

const
  MaxDigitos = 12;
type
   TDisplayInternalState = (csOk, csError);

   IDisplay = Interface
    procedure SetTextDisplay(const Value: String);
    function GetTextDisplay: String;
   End;

   TDisplay = record
    strDisplay: String;
    Display: IDisplay;
    function ExistePuntoDecimal(const APuntoDecimal: Char): Boolean;
  end;

  TCalculadoraBasica = class(TComponent)
  private
    FDisplay: TDisplay;
    FlagNumero: Boolean;
    Operando: Double;
    Operador: String;
    DIS: TDisplayInternalState;
    FCatalogo: TObjectDictionary<String, TDigito>;
    FOnCleanNotifyErrorEvent: TNotifyEvent;
    FOnNotifyErrorEvent: TNotifyEvent;
    FErrorMessage: String;
    FDecimalSeparator: Char;
    FThousandSeparator: Char;
    FDecimalCount: Integer;
    procedure DesplazarDigitoALaIzquierda(AOperando: Char);
    procedure IntroduceDigito(AOperando: Char);
    function EsOperadorInmediato(const ALexema: String): Boolean;
    function GetOperadorText: String;
    procedure SetOnCleanNotifyErrorEvent(const Value: TNotifyEvent);
    procedure SetOnNotifyErrorEvent(const Value: TNotifyEvent);
    procedure SetErrorMessage(const Value: String);
    function GetErrorMessage: String;
    function GetDecimalSeparator: Char;
    function GetThousandSeparator: Char;
    procedure SetDecimalCount(const Value: Integer);
  protected
    procedure ActualizaDisplay(const AOperando: String); virtual;
    procedure DoReset; virtual;
    procedure RegisterCatalogo(ACatalogo: Array of TEtiqueta); virtual;
  public
    constructor Create(AOwner: TComponent; AIDisplay: IDisplay);
    destructor Destroy; override;
    procedure ExceptionToError(E: Exception);
    function ExisteError: Boolean;
    procedure LeeDisplay;
    procedure ProcesarDigito(const ALexema: Char);
    procedure ProcesarOperacion(const ALexema: String);
    procedure RegisterEtiqueta(AEtiqueta: TEtiqueta);
    function Reset: String;
    property DecimalSeparador: Char read GetDecimalSeparator;
    property ThousandSeparator: Char read GetThousandSeparator;
    property DecimalCount: Integer read FDecimalCount write SetDecimalCount;
    property ErrorMessage: String read GetErrorMessage;
    property OperadorText: String read GetOperadorText;
    property OnNotifyErrorEvent: TNotifyEvent read FOnNotifyErrorEvent write SetOnNotifyErrorEvent;
    property OnCleanNotifyErrorEvent: TNotifyEvent read FOnCleanNotifyErrorEvent write SetOnCleanNotifyErrorEvent;
  end;

implementation

uses Rtti;

{ TCalculadoraBasica }

procedure TCalculadoraBasica.ActualizaDisplay(const AOperando: String);
begin
  FDisplay.strDisplay:= AOperando;
end;

constructor TCalculadoraBasica.Create(AOwner: TComponent; AIDisplay: IDisplay);
var
  FS: TFormatSettings;
begin
  FDisplay.Display:= AIDisplay;
  FCatalogo:= TObjectDictionary<String, TDigito>.Create;
  RegisterCatalogo(CatalogoSimbolosBasicos);
  Operando:= 0;
  Operador:= ' ';
  FlagNumero:= False;
  DIS:= csOK;
  FErrorMessage:= '';
  FS:= TFormatSettings.Create;
  FDecimalSeparator:= FS.DecimalSeparator;
  FThousandSeparator:= FS.ThousandSeparator;
  FDecimalCount:= 4;
  FDisplay.strDisplay:= '0';
end;

procedure TCalculadoraBasica.DesplazarDigitoALaIzquierda(AOperando: Char);
begin
  FDisplay.strDisplay:= FDisplay.strDisplay + AOperando;
end;

destructor TCalculadoraBasica.Destroy;
var
  KeyLexema: String;
begin
  for KeyLexema in FCatalogo.Keys do FCatalogo.Items[KeyLexema].Free;
  FCatalogo.Clear;
  FreeAndNil(FCatalogo);
  inherited Destroy;
end;

function TCalculadoraBasica.EsOperadorInmediato(const ALexema: String): Boolean;
begin
  if (ALexema = ' ') or (ALexema = '=') then
    Result:= False
  else Result:= TDigitoOperador(FCatalogo.Items[ALexema]).EsOperadorInmediato;
end;

procedure TCalculadoraBasica.ExceptionToError(E: Exception);
begin
  if Assigned(E) then
  begin
   DIS:= csError;
   FErrorMessage:= E.Message;
   if Assigned(FOnNotifyErrorEvent) then FOnNotifyErrorEvent(Self);
  end;
end;

function TCalculadoraBasica.ExisteError: Boolean;
begin
  Result:= (DIS = csError);
end;

function TCalculadoraBasica.GetDecimalSeparator: Char;
begin
  Result:= FDecimalSeparator;
end;

function TCalculadoraBasica.GetErrorMessage: String;
begin
  Result:= FErrorMessage;
end;

function TCalculadoraBasica.GetOperadorText: String;
begin
  if (Operador = ' ') or (Operador = '=') then
    Result:= Operador
  else Result:= TDigitoOperador(FCatalogo.Items[Operador]).GetRepresentacion;
end;

function TCalculadoraBasica.GetThousandSeparator: Char;
begin
  Result:= FThousandSeparator;
end;

procedure TCalculadoraBasica.IntroduceDigito(AOperando: Char);
begin
  if AOperando = ',' then
    FDisplay.strDisplay:= '0' + AOperando
  else
    FDisplay.strDisplay:= AOperando;

  if FDisplay.strDisplay <> '0' then FlagNumero:= True;
end;

procedure TCalculadoraBasica.LeeDisplay;
var
  FS: TFormatSettings;
begin
  FS:= TFormatSettings.Create;
  FDisplay.Display.SetTextDisplay(Format('%'+Format('%d.%d', [MaxDigitos-FDecimalCount-1,FDecimalCount])+'f', [StrToFloat(FDisplay.strDisplay)], FS));
end;

procedure TCalculadoraBasica.ProcesarDigito(const ALexema: Char);
begin
   if ExisteError then Exit;

   if FlagNumero then
    begin
      if Length(FDisplay.Display.GetTextDisplay) < MaxDigitos then
      begin
         if (FDisplay.strDisplay = '0') and (ALexema = '0') then
           Exit
         else
          begin
            //evaluamos si ya exite el punto decimal
            if (ALexema = DecimalSeparador) and
               (FDisplay.ExistePuntoDecimal(DecimalSeparador)) then
               Exit;

            DesplazarDigitoALaIzquierda(ALexema);
          end;
      end;
    end
   else
    IntroduceDigito(ALexema);

   LeeDisplay;
end;

procedure TCalculadoraBasica.ProcesarOperacion(const ALexema: String);
begin
   if ExisteError then Exit;

   if EsOperadorInmediato(ALexema) then Operador:= ALexema;

   if ((Operador = ' ') or (Operador = '=')) then
     Operando:= StrToFloat(FDisplay.strDisplay)
   else
     try
       Operando:= TDigitoOperador(FCatalogo.Items[Operador]).ExecuteAction(Self, Operando, StrToFloat(FDisplay.strDisplay));
     except
       on E: Exception do if not ExisteError then ExceptionToError(E);
     end;
   ActualizaDisplay(FloatToStr(Operando));
   FlagNumero:= False;
   Operador:= ALexema;

   LeeDisplay;
end;

procedure TCalculadoraBasica.RegisterCatalogo(ACatalogo: array of TEtiqueta);
var
  i: Integer;
begin
  for i:= Low(ACatalogo) to High(ACatalogo) do
    RegisterEtiqueta(ACatalogo[i]);
end;


procedure TCalculadoraBasica.RegisterEtiqueta(AEtiqueta: TEtiqueta);
var
  FClass: TDigitoClass;
begin
  FClass:= AEtiqueta.Clase;
  FCatalogo.Add(AEtiqueta.Lexema, FClass.Create(AEtiqueta, self));
end;

function TCalculadoraBasica.Reset: String;
begin
  DoReset;
  LeeDisplay;
end;

procedure TCalculadoraBasica.SetDecimalCount(const Value: Integer);
begin
  if (Value <> FDecimalCount) then
  begin
    if Value > 0 then
      FDecimalCount := Value
    else FDecimalCount:= 0;
  end;
end;

procedure TCalculadoraBasica.SetErrorMessage(const Value: String);
begin
  FErrorMessage := Value;
end;

procedure TCalculadoraBasica.SetOnCleanNotifyErrorEvent(
  const Value: TNotifyEvent);
begin
  FOnCleanNotifyErrorEvent := Value;
end;

procedure TCalculadoraBasica.SetOnNotifyErrorEvent(const Value: TNotifyEvent);
begin
  FOnNotifyErrorEvent := Value;
end;

procedure TCalculadoraBasica.DoReset;
begin
   if FDisplay.strDisplay = '0' then
    begin
      Operando:= 0;
      Operador:= ' ';
    end
   else
      FDisplay.strDisplay:= '0';
   FlagNumero:= False;
   DIS:= csOK;
   FErrorMessage:= '';
   if Assigned(FOnCleanNotifyErrorEvent) then FOnCleanNotifyErrorEvent(Self);
end;

{ TDisplay }

function TDisplay.ExistePuntoDecimal(const APuntoDecimal: Char): Boolean;
begin
  Result:= (StrScan(Pchar(StrDisplay), APuntoDecimal) <> nil);
end;



end.
