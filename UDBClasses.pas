unit UDBClasses;

interface

uses System.SysUtils, System.Variants, System.Classes;


type
  TDigitoOperador = class;
  TDigitoClass = class of TDigito;

  TEtiqueta = record
    Lexema : String;
    Clase: TDigitoClass;
    Representacion: String;
    EsOperadorInmediato: Boolean;
    function Init: TEtiqueta;
    function Assign(ASource: TEtiqueta): TEtiqueta;
   end;

  TDigito = class(TInterfacedObject)
  private
    FEtiqueta: TEtiqueta;
    FCalculadora: TComponent;
    function GetClass: TDigitoClass;
    function GetEtiqueta: TEtiqueta;
  protected
    procedure DoInit; virtual;
    procedure MessageException(const AMessage:String);
  public
    constructor Create(AEtiqueta: TEtiqueta; ACalculadora: TComponent);
    destructor Destroy; override;
    function ToString: String; override;
    procedure Init;
    function GetRepresentacion: String;
    property ClassOfDigito: TDigitoClass read GetClass;
    property Etiqueta: TEtiqueta read GetEtiqueta;
  end;

  TDigitoOperador = class(TDigito)
  private
  protected
  public
    function DoOperar(AOperador1, AOperador2: Extended): Double; virtual;
    function EsOperadorInmediato: Boolean;
    function ExecuteAction(ACalculadora: TComponent; AOperando1, AOperando2: Double): Double;
  end;

  TDigitoOperadorSuma = class(TDigitoOperador)
  protected
  public
    function DoOperar(AOperador1, AOperador2: Extended): Double; override;
  end;

  TDigitoOperadorResta = class(TDigitoOperador)
  protected
  public
    function DoOperar(AOperador1, AOperador2: Extended): Double; override;
  end;

  TDigitoOperadorMulti = class(TDigitoOperador)
  protected
  public
    function DoOperar(AOperador1, AOperador2: Extended): Double; override;
  end;

  TDigitoOperadorDivision = class(TDigitoOperador)
  protected
  public
    function DoOperar(AOperador1, AOperador2: Extended): Double; override;
  end;


const
  CatalogoSimbolosBasicos: Array[0..3] of TEtiqueta = (
       (Lexema:'x'   ; Clase: TDigitoOperadorMulti;    Representacion: '*'; EsOperadorInmediato: False),           // digito multiplicacion
       (Lexema:'+'   ; Clase: TDigitoOperadorSuma;     Representacion: '+'; EsOperadorInmediato: False),           // digito suma
       (Lexema:'-'   ; Clase: TDigitoOperadorResta;    Representacion: '-'; EsOperadorInmediato: False ),          // digito resta
       (Lexema:'/'   ; Clase: TDigitoOperadorDivision; Representacion: #$2797; EsOperadorInmediato: False));       // digito divide


implementation

{ TDigitoOperador }

uses UDBCalculadora;


function TDigitoOperador.DoOperar(AOperador1, AOperador2: Extended): Double;
begin
end;

function TDigitoOperador.EsOperadorInmediato: Boolean;
begin
  Result:= FEtiqueta.EsOperadorInmediato;
end;

function TDigitoOperador.ExecuteAction(ACalculadora: TComponent;
  AOperando1, AOperando2: Double): Double;
begin
  Result:= DoOperar(AOperando1, AOperando2);
end;

{ TDigitoOperadorSuma }

function TDigitoOperadorSuma.DoOperar(AOperador1, AOperador2: Extended): Double;
begin
  Result:= AOperador1 + AOperador2;
end;

{ TDigitoOperadorResta }

function TDigitoOperadorResta.DoOperar(AOperador1,
  AOperador2: Extended): Double;
begin
  Result:= AOperador1 - AOperador2;
end;

{ TDigitoOperadorMulti }

function TDigitoOperadorMulti.DoOperar(AOperador1,
  AOperador2: Extended): Double;
begin
  Result:= AOperador1 * AOperador2;
end;

{ TDigitoOperadorDivision }

function TDigitoOperadorDivision.DoOperar(AOperador1,
  AOperador2: Extended): Double;
begin
  Result:= 0;

  if AOperador2 = 0 then
     MessageException('Error division por cero')
  else
    Result:= AOperador1 / AOperador2;
end;

{ TEtiqueta }

function TEtiqueta.Assign(ASource: TEtiqueta): TEtiqueta;
begin
  with Self do
  begin
    Lexema             := ASource.Lexema;
    Clase              := ASource.Clase;
    Representacion     := ASource.Representacion;
    EsOperadorInmediato:= ASource.EsOperadorInmediato;
  end;
  Result:= Self;
end;

function TEtiqueta.Init: TEtiqueta;
begin
  with Self do
  begin
    Lexema             := '';
    Clase              := Nil;
    Representacion     := '';
    EsOperadorInmediato:= False;
  end;
  Result:= Self;
end;

{ TDigito }

constructor TDigito.Create(AEtiqueta: TEtiqueta; ACalculadora: TComponent);
begin
  //CodeSite.Send( 'Create' );
  Assert(Assigned(ACalculadora),'Error Instancia Clase TClassCalculadora no definida');

  FCalculadora:= ACalculadora;
  FEtiqueta.Assign(AEtiqueta);
end;

destructor TDigito.Destroy;
begin
  //CodeSite.Send( 'Destroy' );

  FCalculadora:= Nil;
  inherited Destroy;
end;

procedure TDigito.DoInit;
begin
  FEtiqueta.Init;
end;

function TDigito.GetClass: TDigitoClass;
begin
  Result:= FEtiqueta.Clase;
end;

function TDigito.GetEtiqueta: TEtiqueta;
begin
  Result:= FEtiqueta;
end;

function TDigito.GetRepresentacion: String;
begin
  Result:= FEtiqueta.Representacion;
end;

procedure TDigito.Init;
begin
  DoInit;
end;

procedure TDigito.MessageException(const AMessage: String);
begin
  try
     Raise Exception.Create(AMessage)
  except
     on E: Exception do TCalculadoraBasica(FCalculadora).ExceptionToError(E);
  end;
end;

function TDigito.ToString: String;
begin
  Result:= GetRepresentacion;
end;

end.
