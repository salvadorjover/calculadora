unit UDBExtendTypes;

interface

uses System.Classes, UDBClasses;

type
  TDigitoOperadorRaizCuadrada = class(TDigitoOperador)
  public
    function DoOperar(AOperador1, AOperador2: Extended): Double; override;
  end;

const
  EtiquetaRaizCuadrada: TEtiqueta = (Lexema: 'RC';
                                     Clase: TDigitoOperadorRaizCuadrada;
                                     Representacion: #$221A;
                                     EsOperadorInmediato: True);  // digito raiz


implementation

uses Math;

{ TDigitoOperadorRaizCuadrada }

function TDigitoOperadorRaizCuadrada.DoOperar(AOperador1,
  AOperador2: Extended): Double;
begin
   Result:= Sqrt(AOperador2);
end;


end.
