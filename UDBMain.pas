unit UDBMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  UDBCalculadora;

type
   TNewLabel = class(TLabel, IDisplay)
    procedure SetTextDisplay(const Value: String);
    function GetTextDisplay: String;
   end;

  TfrmCalculadora = class(TForm)
    bnSiete: TButton;
    bnOcho: TButton;
    bnNueve: TButton;
    bnCuatro: TButton;
    bnCinco: TButton;
    bnSeis: TButton;
    bnUno: TButton;
    bnDos: TButton;
    bnTres: TButton;
    bnCero: TButton;
    bnDobleCero: TButton;
    bnPuntoDecimal: TButton;
    bnResultado: TButton;
    bnDivision: TButton;
    bnMultiplicacion: TButton;
    bnResta: TButton;
    bnSuma: TButton;
    btnAC: TButton;
    bnRC: TButton;
    lbError: TLabel;
    lbErrorMessage: TLabel;
    lbOperador: TLabel;
    TrBDecimalCount: TTrackBar;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    procedure bnDobleCeroClick(Sender: TObject);
    procedure btnACClick(Sender: TObject);
    procedure btnClick(Sender: TObject);
    procedure bnOperarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure bnPuntoDecimalClick(Sender: TObject);
    procedure TrBDecimalCountChange(Sender: TObject);
  private
    { Private declarations }
    FCalculadora: TCalculadoraBasica;
    FDisplay: TNewLabel;
    function NewDisplay: TNewLabel;
    procedure TeclaRaizCuadradaClick(Sender: TObject);
    procedure VisualizarEstadoErrorOperacion(Sender: TObject);
  public
    { Public declarations }
  end;

var
  frmCalculadora: TfrmCalculadora;

implementation

{$R *.fmx}

uses UDBExtendTypes;

procedure TfrmCalculadora.btnClick(Sender: TObject);
begin
  with FCalculadora do
  begin
    ProcesarDigito((Sender as TButton).Text.Chars[0]);
    if not ExisteError then lbOperador.Text:= '';
  end;
end;

function TfrmCalculadora.NewDisplay: TNewLabel;
begin
   Result:= TNewLabel.Create(Self);

   with Result do
   begin
     Parent:= Self;
     Position.X:= 32;
     Position.Y:= 0;
     Text:= '0';
     Width:= 225;
     TextSettings.HorzAlign:= TTextAlign(2);
     TextSettings.FontColor:=  TAlphaColorRec.Blue;
   end;
end;

procedure TfrmCalculadora.FormCreate(Sender: TObject);
begin
   FDisplay:= NewDisplay;
   FCalculadora:= TCalculadoraBasica.Create(Self, FDisplay);
   with FCalculadora do
   begin
     RegisterEtiqueta(EtiquetaRaizCuadrada);
     OnNotifyErrorEvent:= VisualizarEstadoErrorOperacion;
     OnCleanNotifyErrorEvent:= VisualizarEstadoErrorOperacion;
   end;
   VisualizarEstadoErrorOperacion(FCalculadora);
   bnRC.OnClick:= TeclaRaizCuadradaClick;
   bnRC.Text:= EtiquetaRaizCuadrada.Representacion;
   FCalculadora.LeeDisplay;
end;

procedure TfrmCalculadora.TeclaRaizCuadradaClick(Sender: TObject);
begin
  with FCalculadora do
  begin
    ProcesarOperacion(EtiquetaRaizCuadrada.Lexema);
    lbOperador.Text:= OperadorText;
  end;
end;

procedure TfrmCalculadora.TrBDecimalCountChange(Sender: TObject);
begin
   with FCalculadora do
   begin
     if not ExisteError then
     begin
       DecimalCount:= Trunc(TrBDecimalCount.Value);
       LeeDisplay;
     end;
   end;
end;

procedure TfrmCalculadora.VisualizarEstadoErrorOperacion(Sender: TObject);
begin
  with FCalculadora do
  begin
    lbError.Visible:= ExisteError;
    lbErrorMessage.Text:= ErrorMessage;
  end;
end;

procedure TfrmCalculadora.bnOperarClick(Sender: TObject);
begin
  with FCalculadora do
  begin
     ProcesarOperacion((Sender as TButton).Text.Chars[0]);
     lbOperador.Text:= OperadorText;
  end;
end;

procedure TfrmCalculadora.bnPuntoDecimalClick(Sender: TObject);
begin
  with FCalculadora do
  begin
    ProcesarDigito(DecimalSeparador);
    lbOperador.Text:= OperadorText;
  end;
end;

procedure TfrmCalculadora.bnDobleCeroClick(Sender: TObject);
begin
  with FCalculadora do
  begin
     ProcesarDigito('0');
     ProcesarDigito('0');
     if not ExisteError then lbOperador.Text:= '';
  end;
end;

procedure TfrmCalculadora.btnACClick(Sender: TObject);
begin
  FCalculadora.Reset;
  lbOperador.Text:= '';
  FCalculadora.DecimalCount:= Trunc(TrBDecimalCount.Value);
  FCalculadora.LeeDisplay;
end;



{ TNewLabel }

function TNewLabel.GetTextDisplay: String;
begin
  Result:= self.Text;
end;

procedure TNewLabel.SetTextDisplay(const Value: String);
begin
   Text:= Value;
end;


end.
